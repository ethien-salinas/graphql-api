require('dotenv').config()
import { ApolloServer } from 'apollo-server'
import typeDefs from './schema'
import resolvers from './resolvers'
import createStore from './persistence/connection'
import PostAPI from './datasources/PostAPI'
import AuthAPI from './datasources/AuthAPI'
import UserAPI from './datasources/UserAPI'

const store = createStore()

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    postAPI: new PostAPI({ store }),
    userAPI: new UserAPI({ store }),
    authAPI: new AuthAPI({ store })
  }),
  context: async ({ req, connection }) => {
    return connection
      ? connection.context
      : { token: req.headers.authorization || '' }
  }
})

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`)
})