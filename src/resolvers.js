import { PubSub } from 'apollo-server'

const pubsub = new PubSub()
const POST_ADDED = 'POST_ADDED'

export default {

  Query: {
    getPosts: async (_, __, { dataSources, token }) =>
      await dataSources.authAPI.verifyToken(token)
      && dataSources.postAPI.getAllPost(),
    login: async (_, { email, password }, { dataSources }) =>
      await dataSources.authAPI.getToken({ email, password })
  },

  Mutation: {
    addPost: async (_, { author, comment }, { dataSources, token }) => {
      return await dataSources.authAPI.verifyToken(token)
        && pubsub.publish(POST_ADDED, { postAdded: { author, comment } })
        && dataSources.postAPI.addPost({ author, comment })
    },
    signup: async (_, { name, email, password }, { dataSources }) => {
      return dataSources.userAPI.createUser({ name, email, password })
    }
  },

  Subscription: {
    postAdded: {
      subscribe: () => pubsub.asyncIterator([POST_ADDED])
    },
  }

}
