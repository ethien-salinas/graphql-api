import { gql } from 'apollo-server'

export default gql`

  type Query {
    getPosts: [Post]
    login(email: String, password: String): String
  }

  type Mutation {
    addPost(author: String, comment: String): Post
    signup(name: String, email: String, password: String): User
  }

  type Subscription {
    postAdded: Post
  }

  type Post {
    author: String
    comment: String
  }

  type User {
    id: ID!
    name: String!
    email: String!
  }

`