# GraphQL with Apollo Server 2

## Initial setup
Create your own `.env` file to define environmental values to already defined keys
```sh
$ mv .env-example .env
```

## Start the project
To start the project run
```sh
$ npm start
```
by default the project will start at `http://localhost:4000/`

## Basic `queries` and `mutations`
### login `mutation`
```
mutation{
  login(email: "ethien.salinas@gmail.com", password: "qwerty")
}
```

### getPost `query`
```sh
query{
  getPosts{
    author
    comment
  }
}

{"Authorization": "Bearer your-jwt.from.login-query"}
```

## Protected resources
A `query` or `mutation` which is considered protected (callable only if `authentication token` is present) must follow this configuration
```sh
  protectedResource: async (_, __, { dataSources, token }) =>
    await dataSources.authAPI.verifyToken(token)
    && dataSources.myCustomAPI.myProtectedOperation()
```